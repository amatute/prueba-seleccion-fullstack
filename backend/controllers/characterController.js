const Character = require("./../models/characterModel");

exports.getAllCharacters = async (req, res, next) => {
  const page = req.query.page * 1 || 1;
  const limit = req.query.limit * 1;
  const skip = (page - 1) * limit;

  const characters = await Character.find()
    .skip(skip)
    .limit(limit);
  res.status(200).json({
    status: "success",
    results: characters.length,
    data: {
      characters
    }
  });
};

exports.getCharacter = async (req, res, next) => {
  const character = await Character.findById(req.params.id);
  if (!character) {
    return next(new Error("No character found with that Id", 404));
  }
  res.status(200).json({
    status: "success",
    data: {
      character
    }
  });
};
