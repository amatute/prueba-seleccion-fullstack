const express = require("express");
const mongoose = require("mongoose");
const dbInit = require("./services/dbInit");
const characterRouter = require("./routes/characterRoutes");

var cors = require("cors");

const app = express();
app.use(cors());

app.use(express.json());
app.use(express.static(`${__dirname}/public`));

// Data Base

const DB_CONNECTION =
  "mongodb+srv://amatute:WP3RshMka2os0cBN@cluster0-ygzug.mongodb.net/got-test?retryWrites=true&w=majority";

mongoose
  .connect(DB_CONNECTION, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  })
  .then(() => console.log("DB connected 😊"));

//Get all characters from public api and save them in a mongo database
dbInit.getAllCharacters();

// Routes
app.use("/characters", characterRouter);

const port = process.env.PORT || 4000;

app.listen(port, () => {
  console.log(`App listening on port ${port}...`);
});
