const axios = require("axios");
const Character = require("../models/characterModel");

exports.getAllCharacters = async () => {
  try {
    const charactersInDB = await Character.find();

    if (charactersInDB.length === 0) {
      const res = await axios.get(
        "https://api.got.show/api/general/characters"
      );

      const allCharacters = Object.values(res.data.book).map(character => ({
        name: character.name,
        house: character.house,
        image: character.image,
        gender: character.gender,
        slug: character.slug,
        titles: character.titles,
        books: character.books,
        rank: character.pagerank.rank
      }));

      await Character.insertMany(allCharacters, err => {
        if (err) {
          console.log(err);
        } else console.log("Data saved!");
      });
    } else {
      console.log("BD already populated.");
    }
  } catch (error) {
    console.log("Error, characters unavailable");
  }
};
