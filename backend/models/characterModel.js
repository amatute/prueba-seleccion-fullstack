const mongoose = require("mongoose");

const characterSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true
  },
  house: String,
  image: String,
  gender: String,
  slug: String,
  titles: [],
  books: [],
  rank: String
});

const Character = mongoose.model("Character", characterSchema);

module.exports = Character;
