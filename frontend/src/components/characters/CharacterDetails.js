import React from "react";

const CharacterDetails = props => {
  const {
    name,
    image,
    house,
    gender,
    slug,
    rank,
    titles,
    books
  } = props.location.state.character;

  return (
    <div>
      <h2>{name}</h2>
      {image ? (
        <img src={image} alt="" height="200" width="200"></img>
      ) : (
        <p>No hay imagen disponible</p>
      )}
      <p>Casa: {house}</p>
      <p>Sexo: {gender}</p>
      <p>Slug: {slug}</p>
      <p>Rango: {rank}</p>
      <p>Titulos:</p>
      <ul>
        {titles.map(title => (
          <li key={title}>{title}</li>
        ))}
      </ul>
      <p>Libros:</p>
      <ul>
        {books.map(book => (
          <li key={book}>{book}</li>
        ))}
      </ul>
    </div>
  );
};

export default CharacterDetails;
