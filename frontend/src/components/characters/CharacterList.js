import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Pagination from "./../Pagination";

const CharacterList = () => {
  const [characters, setCharacters] = useState([]);
  const [searchString, setSearchString] = useState("");

  const [searchResults, setSearchResults] = useState([]);

  const [currentPage, setCurrentPage] = useState(1);
  const [charactersPerPage] = useState(10);

  const handleChange = e => {
    setSearchString(e.target.value.toLowerCase());
  };

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios("http://localhost:4000/characters");
      setCharacters(result.data.data.characters);
    };
    fetchData();
  }, []);

  useEffect(() => {
    const results = characters.filter(
      character =>
        character.name.toLowerCase().includes(searchString) ||
        (character.house
          ? character.house.toLowerCase().includes(searchString)
          : true)
    );
    setCurrentPage(1);
    setSearchResults(results);
  }, [searchString, characters]);

  const paginate = pageNumber => setCurrentPage(pageNumber);
  const indexOfLastCharacter = currentPage * charactersPerPage;
  const indexOfFirsCharacter = indexOfLastCharacter - charactersPerPage;

  return (
    <div>
      <h2>Game of Thrones</h2>
      <h4>Lista de personajes:</h4>
      <label htmlFor="search">Buscar:</label>
      <input
        type="text"
        id="search"
        name="search"
        onChange={handleChange}
      ></input>
      <br></br>
      <ul>
        {searchResults
          .slice(indexOfFirsCharacter, indexOfLastCharacter)
          .map(character => (
            <div key={character._id}>
              <Link
                to={{
                  pathname: "/characters/" + character._id,
                  state: {
                    character: character
                  }
                }}
              >
                {character.name}
              </Link>
            </div>
          ))}
      </ul>

      <Pagination
        charatersPerPage={charactersPerPage}
        totalCharacters={searchResults.length}
        paginate={paginate}
      />
    </div>
  );
};

export default CharacterList;
