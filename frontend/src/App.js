import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import CharacerList from "./components/characters/CharacterList";
import CharacterDetails from "./components/characters/CharacterDetails";
import NotFoundPage from "./components/NotFoundPage";

const App = () => (
  <BrowserRouter>
    <div className="App">
      <Switch>
        <Route path="/characters/:id" component={CharacterDetails} />
        <Route path="/" component={CharacerList} />
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  </BrowserRouter>
);

export default App;
